const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8081;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_back_end"
});
connection.connect(function(err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS participanti_inscrisi(nume VARCHAR(255),prenume VARCHAR(255),telefon VARCHAR(255),email VARCHAR(255),ocupatie VARCHAR(255),varsta INTEGER)";
  connection.query(sql, function(err, result) {
    if (err) throw err;
  });
});
app.post("/inscriere", (req, res) => {
    const participant ={
        nume: req.body.nume,
        prenume: req.body.prenume,
        telefon:req.body.telefon,
        email:req.body.email,
        ocupatie: req.body.ocupatie,
        varsta: req.body.varsta    
    }
  let error = [];

  if (!participant.nume||!participant.prenume||!participant.telefon||!participant.email||!participant.ocupatie||!participant.varsta) {
    error.push("Unul sau mai multe campuri nu au fost introduse");
    console.log("Unul sau mai multe campuri nu au fost introduse!");
  } else {
    if (participant.nume.length < 2 || participant.nume.length > 30) {
      console.log("Nume invalid!");
      error.push("Nume invalid");
    } else if (!participant.nume.match("^[A-Za-z]+$")) {
      console.log("Numele trebuie sa contina doar litere!");
      error.push("Numele trebuie sa contina doar litere!");
    }
    if (participant.prenume.length < 2 || participant.nume.length > 30) {
      console.log("Prenume invalid!");
      error.push("Prenume invalid!");
    } else if (!participant.prenume.match("^[A-Za-z]+$")) {
      console.log("Prenumele trebuie sa contina doar litere!");
      error.push("Prenumele trebuie sa contina doar litere!");
    }
    if (participant.telefon.length != 10) {
      console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
      error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
    } else if (!participant.telefon.match("^[0-9]+$")) {
      console.log("Numarul de telefon trebuie sa contina doar cifre!");
      error.push("Numarul de telefon trebuie sa contina doar cifre!");
    }
    if (!participant.email.includes("@gmail.com") && !participant.email.includes("@yahoo.com")) {
      console.log("Email invalid!");
      error.push("Email invalid!");
    }
    if(!participant.ocupatie.match("^[A-Za-z]+$")) {
        console.log("Denumirea ocupatiei trebuie sa contina doar litere!");
        error.push("Denumirea ocupatiei trebuie sa contina doar litere!");
    }
    
    if (parseInt(participant.varsta) === "NaN") {
      console.log("Varsta trebuie sa contina doar cifre!");
      error.push("Varsta trebuie sa contine doar cifre!");
    } 
        
  }
  if (error.length === 0) {
    const sql =
      `INSERT INTO participanti_inscrisi (nume,prenume,telefon,email,ocupatie,varsta) VALUES (?,?,?,?,?,?)`;
    connection.query(sql,
        [
        participant.nume, 
        participant.prenume, 
        participant.telefon, 
        participant.email, 
        participant.ocupatie,  
        participant.varsta  
        ],
        function(err, result) {
            if (err) throw err;
            console.log("participant inscris in baza de date!");
            res.status(200).send({
                message: "participant inscris in baza de date!"
            });
        console.log(sql);
    });
  } else {
    res.status(500).send(error);
    console.log("Eroare la inserarea in baza de date!");
  }

});
